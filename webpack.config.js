
const path = require('path');

function resolve(dir) {
    return path.resolve(__dirname, dir)
}

function join (dir) {
    return path.join(__dirname, dir)
}

// 引入htmlwebpackplugins
var HtmlWebpackPlugin = require('html-webpack-plugin');

// 引入webpack
const webpack = require('webpack');

// 引入VueLoaderPlugin
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    entry: './src/main.js',
    output: {
        filename: 'bundle.js',
        path: resolve('dist')
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                include: resolve('src'),
                use: ['vue-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                include: resolve('src'),
                use: ['style-loader','css-loader']
            },
            {
                test: /\.(png|jpe?g|gif)/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            name: '[name].[hash:8].[ext]',
                            outputPath: 'images/'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),

        // 引入热更新模块
        new webpack.HotModuleReplacementPlugin(),

        // 引入 VueLoaderPlugin
        new VueLoaderPlugin()
    ],
    devServer: {
        contentBase: join('dist'),
        compress: true,
        port: 9999,

        // 开启热更新
        hot: true
    }
}
