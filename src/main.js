import {add} from './js/math'

import './css/index.css'


console.log(add(3, 4))

import Vue from 'vue'
import App from './App.vue'

new Vue({
    el: '#app',
    render: h => h(App),
})